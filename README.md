# headers-injection-proxy

Just a simple proxy for injecting headers into requests. It handles redirects. It ignores SSL. No configuration, no bullshit!

### Installing

```
npm install -g headers-injection-proxy
```

### Usage

```
headers-injection-proxy -p [proxy port] -t [host you want to inject headers] -h [headers json file path]
```
### Example

```
headers-injection-proxy -p 3000 -t "https://www.google.com" -h "headers.json"
```

### Headers config file example

```
{
    "header1" : "test1",
    "header2" : "test2"
}
```


## Authors

* **Maurizio Macrì**
## License

This project is licensed under the ISC License
